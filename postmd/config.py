"""Global config"""
import os
import sys
import logging


DEFAULTS = {
    'LOG_FORMAT': '{levelname}|{asctime}|{module}.{funcName}|{message}',
    'LOG_LEVEL': 'INFO',
    'DATA_DIR': os.path.join(os.path.dirname(__file__), 'data'),
}


def get(name, default=None):
    """
    Get the config value for `name`, by first checks `name` in environment
    variables, then using predefined defaults.
    """
    if name in os.environ:
        return os.environ[name]
    elif default is not None:
        return default
    elif name in DEFAULTS:
        return DEFAULTS[name]
    else:
        raise KeyError(name)


# set basic loggin information

logging.basicConfig(
	level=get('LOG_LEVEL'),
	stream=sys.stdout,
	format=get('LOG_FORMAT'),
	style='{'
)


"""
Minimum setup.py framework
"""
import os
from setuptools import setup, find_packages

PACKAGE_NAME = 'postmd'
PACKAGE_URL = 'https://bitbucket.org/postmd/postmd/'

with open(os.path.join(PACKAGE_NAME, '__version__.py'), 'r') as fd:
    PACKAGE_VERSION = fd.read().strip().split()[-1].strip("'")

# minimum setup
setup(
    name=PACKAGE_NAME,
    version=PACKAGE_VERSION,
    url=PACKAGE_URL,
    include_package_data=True,
    packages=find_packages(),
    package_data={
        PACKAGE_NAME: ['data/*.zip']
    },
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'pytest-cov']
)
